# Настройка для балансировщика
## Предусловия
1. У вас есть несколько нод с установленным MinioS3
2. У вас есть настроенные домены к примеру  
`minio-1.domain.com` `minio-2.domain.com`
3. У вас есть основной endpoint для балансировщика nginx например:  
`minio.domain.com`

*Вся настройка относится к серверу балансировщика `minio.domain.com`*  

## Установка и настройка nginx

Установите (если необходимо) нужные пакеты

```bash
sudo apt install software-properties-common
```

```bash
sudo add-apt-repository universe
```

```bash
sudo add-apt-repository ppa:certbot/certbot
```

```bash
sudo apt update
```
### Установим certbot для генерации сертификата
```bash
sudo apt install certbot
```
### Установим nginx и нужные библиотеки для certbot

```bash
sudo apt install nginx python3-certbot-nginx
```

### Сгенерируем сертификаты

```bash
sudo certbot certonly --standalone -d minio.domain.com
```

*Выполните все нужные операции при установке такие как указать емайл, согласится с правилами и т.д.*

## Проверка необходимых настроек

Проверьте есть ли у вас настройки безопасности ssl  
`/etc/letsencrypt/options-ssl-nginx.conf` и `/etc/letsencrypt/ssl-dhparams.pem`

### Если необходимые файлы отсутствуют

>Эти файлы используются в конфигурации NGINX для повышения безопасности соединений SSL/TLS. Они обычно создаются при настройке Certbot с флагом --nginx. Если эти файлы не существуют, вы можете создать их вручную или использовать рекомендованные параметры.

#### Создание файла `options-ssl-nginx.conf`

```bash
sudo nano /etc/letsencrypt/options-ssl-nginx.conf
```

Запишите в файле 
```nginx
ssl_protocols TLSv1.2 TLSv1.3;
ssl_prefer_server_ciphers on;
ssl_ciphers "ECDHE-ECDSA-CHACHA20-POLY1305:ECDHE-RSA-CHACHA20-POLY1305:ECDHE-ECDSA-AES128-GCM-SHA256:ECDHE-RSA-AES128-GCM-SHA256:ECDHE-ECDSA-AES256-GCM-SHA384:ECDHE-RSA-AES256-GCM-SHA384:DHE-RSA-AES128-GCM-SHA256:DHE-RSA-AES256-GCM-SHA384";
ssl_ecdh_curve secp384r1; # Requires nginx >= 1.1.0
ssl_session_timeout  10m;
ssl_session_cache shared:SSL:10m;
ssl_session_tickets off; # Requires nginx >= 1.5.9
ssl_stapling on; # Requires nginx >= 1.3.7
ssl_stapling_verify on; # Requires nginx => 1.3.7
resolver 8.8.8.8 8.8.4.4 valid=300s;
resolver_timeout 5s;
add_header Strict-Transport-Security "max-age=63072000; includeSubdomains; preload" always;
add_header X-Frame-Options DENY;
add_header X-Content-Type-Options nosniff;
```

#### Создание файла `ssl-dhparams.pem`

```bash
sudo openssl dhparam -out /etc/letsencrypt/ssl-dhparams.pem 2048
```

>Это создаст файл с параметрами Диффи-Хеллмана для усиления шифрования.

Все готово, используйте [файл nginx для настройки сервера балансировки (измените пути на ваши)](./nginx.balance.conf)

-------
# Если вы не знаете куда и как сохранять настройки nginx читайте ниже, если знаете - тогда не читайте

## Создайте файл конфигурации
Скопируйте конфиг из файла [nginx.balance.conf](./nginx.balance.conf) в файл на сервере `/etc/nginx/sites-available/minio`

```bash
sudo nano /etc/nginx/sites-available/minio
```
## Добавьте ссылку на файл в директорию `/etc/nginx/sites-enabled/

```bash
sudo ln -s /etc/nginx/sites-available/minio /etc/nginx/sites-enabled/
```

 ## Проверьте конфигурацию
 ```bash
sudo nginx -t
 ```

 ## Перезапустите nginx
 ```bash
sudo systemctl restart nginx
 ```
-------
 # Результат

 ## Доступ к api
 `https://minio.domain.com`

 ## Доступ к консоли MinioS3
 `https://minio.domain.com:8080`