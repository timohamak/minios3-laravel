# Minios3
Содержание процесса установки и настройки MinioS3 локально для разработки

1. Скачать репозиторий [Репозиторий](https://gitlab.com/timohamak/minios3-laravel/-/tree/master?ref_type=heads) 
2. Запустить docker-compose up -d
3. В проекте (СРМ или другой) Установить AWS SDK для Laravel  (5.6) ```composer require league/flysystem-aws-s3-v3:^1.0```
4. На локальной машине добавить домен (hosts)
5. В Laradock добавить домен minio.minio.local (или свой локальный)

-----------
# Настройка MinioS3
-----------
## Скачать и запустить репозиторий
1. Клонировать репозиторий

`git clone git@gitlab.com:timohamak/minios3-laravel.git`

2. Настроить .env есть example.env для примера можно не менять если порты 9100 и 9101 не заняты, а также для nginx порты `8090 4431 8021`

3. Добавить хост в hosts на локальной машине `/etc/hosts`

`127.0.0.1  minio.minio.local`

4. Запустить команду `docker-compose up -d` 

У вас будет запущен сервер minios3 на локальной машине

----------
# Подключение к MinioS3
----------
## Настройки для CRM
Должна быть слита ветка с настройками minios3 (если она не в проде есть ветка dev712 на момент написания) если другой проект [смотрите детальную настройку laravel](./README.md)

В Laradock окружении надо добавить ваш домен в файле `/laradock/docker-compose.yml` для сервисов `workspace` и `php-fpm`

Пример:

```yml
extra_hosts:
        ...
        - "minio.minio.local:${INTERNAL_IP}"
```

Сделайте билд `docker-compose build php-fpm workspace`

Ваш minios3 должен быть доступе по адресу [minio console](http://minio.minio.local:9101/) логин и пароль из .env `miniotest miniotest`

В консоли минио создайте ключи для срм или другого сервиса (secret key) и (access_key) [minio console](http://minio.minio.local:9101/access-keys)

Добавьте эти ключи в .env вашего сервиса 

Пример для Laravel CRM

```env
#AWS S3
AWS_ACCESS_KEY_ID=MSq7xCRfrjffjLUoQV5C
AWS_SECRET_ACCESS_KEY=ET6VKzFiSaApV1FMxLz4iNZYYdygf0xqlob4WGEy
AWS_DEFAULT_REGION=us-east-1
AWS_BUCKET=default
AWS_URL=http://minio.minio.local:9100
AWS_ENDPOINT=http://minio.minio.local:9100
AWS_USE_PATH_STYLE_ENDPOINT=true
```

## Использование 
 ```php
//Записать файл
Storage::disk('s3')->put('template.xlsx', $fileContents);
//Получить временную ссылку на файл на 360 минут
$result= Storage::disk('s3')->temporaryUrl($file, now()->addMinutes(360));
//Получить прямую ссылку 
$result= Storage::disk('s3')->url($file);
 ```

> — by Artem M. 