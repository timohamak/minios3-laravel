# Minios3
Содержание процесса установки и настройки MinioS3

[См. краткое руководство здесь](./README-SHORT.md)

1. Установить Docker MinioS3 
2. Установить AWS SDK для Laravel ```composer require league/flysystem-aws-s3-v3:^1.0```
3. Добавить нового провайдера Laravel provider
4. Laradock добавить домен minio.minio.local (или свой локальный)

# Настройка MinIO с самоподписанным сертификатом через Docker Compose и Nginx

## 1 `docker-compose.yml`

добавить самоподписанный сертификат они лежат в папке .minio/certs/ private.key и public.crt

### `docker-compose.yml`, указываем домен и монтируем сертификаты:

```yaml
version: '3.7'

services:
  minio:
    image: minio/minio
    container_name: minio
    environment:
      MINIO_ROOT_USER: ${MINIO_ROOT_USER}
      MINIO_ROOT_PASSWORD: ${MINIO_ROOT_PASSWORD}
    command: server --address ":${MINIO_PORT}" --console-address ":${MINIO_CONSOLE_PORT}" /data
    volumes:
      - ./minio-data:/data
    ports:
      - "${MINIO_PORT}:9100"
      - "${MINIO_CONSOLE_PORT}:9101"
    networks:
      - minio-network

  nginx:
    image: nginx:latest
    container_name: nginx
    ports:
      - "8090:80"
      - "4431:443"
    volumes:
      - ./nginx.conf:/etc/nginx/nginx.conf
      - ./certs:/etc/letsencrypt
      - ./certs-data:/data/letsencrypt
      - ./.minio/certs:/etc/nginx/certs # Монтирование сертификатов
    depends_on:
      - minio
    networks:
      - minio-network

volumes:
  minio-data:
  certs-data:

networks:
  minio-network:
    driver: bridge
```
## 1.1. Файл .env MinioS3

_Пароль должен быть не менее 8 символов_

```bash
MINIO_ROOT_USER=miniotest
MINIO_ROOT_PASSWORD=miniotest
MINIO_DEFAULT_BUCKETS=default
MINIO_USE_SSL=true
MINIO_STORAGE_LOCATION="./data"
MINIO_PORT=9100
MINIO_CONSOLE_PORT=9101
```

## 2. Конфигурация Nginx (nginx.conf)
Файл ```nginx.conf``` должен находится в одной директории с ```docker-compose.yml```
```nginx
events { }

http {
    server {
        listen 8090;
        server_name minio.minio.local;

        location / {
            proxy_pass http://minio:9100;
            proxy_set_header Host $host;
            proxy_set_header X-Real-IP $remote_addr;
            proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
            proxy_set_header X-Forwarded-Proto $scheme;
        }

        location /minio {
            proxy_pass http://minio:9101;
            proxy_set_header Host $host;
            proxy_set_header X-Real-IP $remote_addr;
            proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
            proxy_set_header X-Forwarded-Proto $scheme;
        }

        location /.well-known/acme-challenge/ {
            root /data/letsencrypt;
        }
    }

    server {
        listen 4431 ssl;
        server_name minio.minio.local;

        ssl_certificate /etc/nginx/certs/public.key;
        ssl_certificate_key /etc/nginx/certs/private.key;

        location / {
            proxy_pass http://minio:9100;
            proxy_set_header Host $host;
            proxy_set_header X-Real-IP $remote_addr;
            proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
            proxy_set_header X-Forwarded-Proto $scheme;
        }

        location /minio {
            proxy_pass http://minio:9101;
            proxy_set_header Host $host;
            proxy_set_header X-Real-IP $remote_addr;
            proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
            proxy_set_header X-Forwarded-Proto $scheme;
        }
    }
}
```
## 3. Настройка DNS
Обновите файл /etc/hosts на вашей машине (или на всех машинах, которые будут использовать этот домен) для указания на IP-адрес вашего сервера

```lua
localhost minio.minio.local
```

## 4. Запуск Docker Compose

Запустите все сервисы с помощью Docker Compose:

```sh
docker-compose up -d
```

## 5. Проверка доступа
Теперь вы должны иметь возможность получить доступ к MinIO через ваш локальный домен:

WebUI будет доступен по адресу http://minio.minio.local:9101 

## 6. .env для CRM

```AWS_ACCESS_KEY_ID``` и ```AWS_SECRET_ACCESS_KEY``` нужно создать в кабинете MinioS3 по вашему адресу [Minio Console](http://minio.minio.local:9101)

```env
#AWS S3
AWS_ACCESS_KEY_ID=MSq7xCRfrjffjLUoQV5C
AWS_SECRET_ACCESS_KEY=ET6VKzFiSaApV1FMxLz4iNZYYdygf0xqlob4WGEy
AWS_DEFAULT_REGION=us-east-1
AWS_BUCKET=default
AWS_URL=http://minio.minio.local:9100
AWS_ENDPOINT=http://minio.minio.local:9100
AWS_USE_PATH_STYLE_ENDPOINT=true
```

## 7. Установка SDK S3
 * AWS SDK - [php 8^](https://flysystem.thephpleague.com/docs/adapter/aws-s3-v3/)
 * AWS SDK - [php 7^](https://flysystem.thephpleague.com/v1/docs/adapter/aws-s3-v3/)

```sh
composer require [ваша версия]
```

## 8. Laravel Service Provider

```sh
php artisan make:provider MinioStorageServiceProvider
```
 Код ```MinioStorageServiceProvider.php``` в папке ```app/Providers/``` 

 ```php
<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

use Aws\S3\S3Client;
use League\Flysystem\AwsS3v3\AwsS3Adapter;
use League\Flysystem\Filesystem;
use Storage;

class MinioStorageServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        Storage::extend('s3', function ($app, $config) {
            $client = new S3Client([
                'credentials' => [
                    'key'    => $config["key"],
                    'secret' => $config["secret"]
                ],
                'region'      => $config["region"],
                'version'     => "latest",
                'bucket_endpoint' => false,
                'use_path_style_endpoint' => true,
                'endpoint'    => $config["endpoint"],
            ]);
            $options = [
                'override_visibility_on_copy' => true
            ];
            return new Filesystem(new AwsS3Adapter($client, $config["bucket"], '', $options));
        });
    }

    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}

 ```

 ## 9. Регистрируем провайдер
 В папке ```config/app.php``` в разделе ```providers```

 ```php
/*
* AWS S3 Service Provider
*/

App\Providers\MinioStorageServiceProvider::class
 ```

 ## 10. Настройки драйвера

 В файле ```config/filesystems.php```

 ```php
's3' => [
    'driver' => 's3',
    'key' => env('AWS_ACCESS_KEY_ID'),
    'secret' => env('AWS_SECRET_ACCESS_KEY'),
    'region' => env('AWS_DEFAULT_REGION', ''),
    'bucket' => env('AWS_BUCKET'),
    'url' => env('AWS_URL'),
    'endpoint' => env('AWS_ENDPOINT', env('AWS_URL')),
    'bucket_endpoint' => false,
    'use_path_style_endpoint' => env('AWS_USE_PATH_STYLE_ENDPOINT', true),
    'throw' => true,
],
 ```

Использование 
 ```php
//Записать файл
Storage::disk('s3')->put('template.xlsx', $fileContents);
//Получить временную ссылку на файл
$result= Storage::disk('s3')->temporaryUrl($file, now()->addMinutes(360));
//Получить прямую ссылку 
$result= Storage::disk('s3')->url($file);
 ```

> — by Artem M. 